<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%iS5m+j-Xdqy. jAiD,x|mRuQR*Dfsb<nE(qADZ`P-y|@e AvV}Xq8Lf/*2Tx6To');
define('SECURE_AUTH_KEY',  '|gv+^PN268O8Crkw=4 1ib]HMm/P~-/gJKW*)%x.=JAzEXGn;phQ>o=EvNY]=OF^');
define('LOGGED_IN_KEY',    '*+E74^fS;sDm0WM/wNxTQkX]G/+rplcGK49:Rcre4-<A7GJjTm4_=3g2S?ESv~m;');
define('NONCE_KEY',        'Cy|W+oDPd/YdcNwC8BAq]9I*LPwNl`yP+02%8NE@t7W:Nl,<f}Zo!KvaB;FMWI5q');
define('AUTH_SALT',        'its(t`g`xd^R1CKW.80fFl6;p_F{2lchBc(u&l4k*.#AA)K.XV$V9W/vh#B]?e@y');
define('SECURE_AUTH_SALT', 'OVB)#![Kd06OEp[aTyY$}3qROt%U]^,;JMc!_W5$]pKaXC(t{3c`*)*A;aes|2P`');
define('LOGGED_IN_SALT',   'q|G+HsLG*y#S!!]@U-/M,mo{~F9ehcI2j<Ds%4tN2FtQ|1hUbH<o>/*#0c$(YZ^,');
define('NONCE_SALT',       ' exP7|gGPND}S:h5YLCUs_@i<[;mV!#|U`p*8vLa/N:5piZvKKTrG*S88}2R%FE-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
